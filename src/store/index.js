import { createStore } from "vuex";
import axios from "axios";

export default createStore({
  state: {
    tickerValue: null,
    tickerList: [],
    latestTickerValue: {},
    tickerName: "AAPL",
    finances: null,
    balanceSheet: null,
    cashFlow: null,
    tickerInfo: null,
    demoKey: "A6HJVHM1RNAYE5PL"
  },
  mutations: {
    setTicker: (state, tickerValue) => (state.tickerValue = tickerValue),
    setTickerName: (state, tickerName) => (state.tickerName = tickerName),
    setTickersList: (state, tickerList) => (state.tickerList = tickerList),
    setTickerInfo: (state, tickerInfo) => (state.tickerInfo = tickerInfo),
    setInterval: (state, interval) => (state.interval = interval),
    setFinances: (state, finances) => (state.finances = finances),
    setBalanceSheet: (state, balanceSheet) =>
      (state.balanceSheet = balanceSheet),
    setCashFlow: (state, cashFlow) => (state.cashFlow = cashFlow),
    setLatestValue: (state, latestTickerValue) =>
      (state.latestTickerValue = latestTickerValue)
  },

  actions: {
    async fetchTicker({ commit, state, dispatch }, ticker = null) {
      ticker = ticker ? ticker.toUpperCase() : state.tickerName;

      try {
        const { data } = await axios.get(
          `https://www.alphavantage.co/query?function=TIME_SERIES_INTRADAY&symbol=${state.tickerName}&interval=5min&outputsize=full&apikey=${state.demoKey}`
        );

        const values = data[`Time Series (5min)`];
        let arr = Object.entries(values);

        arr = arr.map((el) => {
          return {
            date: el[0],
            open: el[1]["1. open"],
            high: el[1]["2. high"],
            low: el[1]["3. low"],
            close: el[1]["4. close"],
            volume: el[1]["5. volume"]
          };
        });

        commit("setTicker", arr);
        commit("setTickerName", ticker);
        dispatch("fetchTickerInfo");
        dispatch("fetchLatestValue");
        dispatch("fetchFinances");
        dispatch("fetchBalanceSheet");
        dispatch("fetchCashFlow");
      } catch (error) {
        return;
      }
    },

    async fetchTickerList({ commit, state }) {
      try {
        const res = await axios.get(
          `https://www.alphavantage.co/query?function=LISTING_STATUS&apikey=${state.demoKey}`
        );

        console.log("All Listed companies ----->", res.data);

        commit("setTickersList", res.data);
      } catch (error) {}
    },

    fetchLatestValue({ commit, state }) {
      const latestIndex = state.tickerValue.length - 1;
      const latestData = state.tickerValue[latestIndex];

      console.log(latestData);

      commit("setLatestValue", latestData);
    },

    async fetchTickerInfo({ commit, state }) {
      try {
        const res = await axios.get(
          `https://www.alphavantage.co/query?function=OVERVIEW&symbol=${state.tickerName}&apikey=${state.demoKey}`
        );

        console.log("Ticker info ----->", res.data);

        commit("setTickerInfo", res.data);
      } catch (error) {}
    },

    async fetchFinances({ commit, state }) {
      try {
        const res = await axios.get(
          `https://www.alphavantage.co/query?function=INCOME_STATEMENT&symbol=${state.tickerName}&apikey=${state.demoKey}`
        );

        console.log("FINANCES ----->", res.data);

        commit("setFinances", res.data);
      } catch (error) {}
    },

    async fetchBalanceSheet({ commit, state }) {
      try {
        const res = await axios.get(
          `https://www.alphavantage.co/query?function=BALANCE_SHEET&symbol=${state.tickerName}&apikey=${state.demoKey}`
        );

        console.log("BALANCE SHEET ----->", res.data);

        commit("setBalanceSheet", res.data);
      } catch (error) {}
    },

    async fetchCashFlow({ commit, state }) {
      try {
        const res = await axios.get(
          `https://www.alphavantage.co/query?function=CASH_FLOW&symbol=${state.tickerName}&apikey=${state.demoKey}`
        );

        console.log("CASH FLOW ----->", res.data);

        commit("setCashFlow", res.data);
      } catch (error) {}
    },

    setInterval(interval = "5min") {
      commit("setInterval", interval);
    }
  },
  modules: {}
});
